DNS
===

This component is responsible for configuring DNS resolution services for
consumption by other members of the internal (not cluster) network.

The service in use is CoreDNS, which is configured with several zones. It is
exposed using a LoadBalancer type service with a fixed IP, matching the
``kube-vip``.

Malware & Advertisement Denylist
--------------------------------

`Pi-Hole <https://pi-hole.net/>`_ is a popular project which uses a DNS proxy
with a built-in denylist to prevent DNS lookups of malware and advertising
domains. This works as an additional layer of protection for devices which do
not cooperate with adblockers, and for sites which work around them.

This component does not use Pi-Hole, but leverages a similar mechanism: CoreDNS
is able to resolve against a local ``hosts``-style file; we use an
``initContainer`` and script to build such a list dynamically at pod
initialization time with garbage resolve-to addresses, and have CoreDNS use
that list ahead of forwarding any resolution attempts.

The deny list is built using the below script, adapted from `this ycombinator
comment <https://news.ycombinator.com/item?id=21238213>`_.

.. collapse:: Code

  .. literalinclude:: build-denylist.sh
    :language: bash

Terragrunt Configuration
------------------------

.. literalinclude:: terragrunt.hcl
  :caption: ``terragrunt.hcl``

Terraform Configuration
-----------------------

.. literalinclude:: coredns.tf
  :caption: ``coredns.tf``
