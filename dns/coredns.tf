variable "domain" {
  type = string
}
variable "service_sharing_key" {
  type    = string
  default = null
}
variable "upstream_nameservers" {
  type = list(string)
}
variable "static_ips" {
  type = map(string)
}
variable "dhcp_resolver" {
  type = string
}
variable "k8s_ingress_ip" {
  type = string
}

locals {
  dhcp_domain         = "dhcp.${var.domain}"
  dhcp_domain_escaped = replace(local.dhcp_domain, ".", "\\.")

  static_domain = "static.${var.domain}"
  k8s_domain    = "k8s.${var.domain}"
}

resource "kubernetes_namespace" "coredns" {
  metadata {
    name = "coredns"
  }
}

resource "kubernetes_config_map" "build_denylist" {
  metadata {
    name      = "build-denylist"
    namespace = one(kubernetes_namespace.coredns.metadata).name
  }

  data = {
    "build-denylist.sh" = file("${path.module}/build-denylist.sh")
  }
}

resource "helm_release" "coredns" {
  name       = "coredns"
  repository = "https://alexander-bauer.github.io/coredns-helm"
  chart      = "coredns"
  namespace  = kubernetes_namespace.coredns.metadata[0].name
  version    = "1.19.21"

  set {
    name  = "isClusterService"
    value = false
  }

  values = [
    yamlencode({
      isClusterService = false
      serviceType      = "LoadBalancer"
      service = {
        annotations = var.service_sharing_key != null ? {
          "metallb.universe.tf/allow-shared-ip" : var.service_sharing_key
        } : {}
      }
      dnsPolicy = "None"
      dnsConfig = {
        nameservers = var.upstream_nameservers
      }
      extraVolumes = [
        {
          name     = "denylist"
          emptyDir = {}
        },
        {
          name = "build-denylist"
          configMap = {
            name = one(kubernetes_config_map.build_denylist.metadata).name
          }
        },
      ]
      extraVolumeMounts = [
        {
          name      = "denylist"
          mountPath = "/denylist"
        }
      ]
      initContainers = [{
        name            = "build-denylist"
        image           = "alpine"
        imagePullPolicy = "IfNotPresent"
        command = ["sh", "-c", <<-EOF
            apk add --no-cache bash curl && bash /usr/local/bin/build-denylist.sh /denylist/hosts.denylist
          EOF
        ]
        volumeMounts = [
          {
            name      = "build-denylist"
            mountPath = "/usr/local/bin"
            readOnly  = true
          },
          {
            name      = "denylist"
            mountPath = "/denylist"
          },
        ]
      }]
      servers = [
        {
          zones = [{ zone = "." }]
          port  = 53
          plugins = [
            { name = "errors" },
            # Serves a /health endpoint on :8080, required for livenessProbe
            { name = "health", configBlock = "lameduck 5s" },
            # Serves a /ready endpoint on :8181, required for readinessProbe
            { name = "ready" },
            # Serves a /metrics endpoint on :9153, required for serviceMonitor
            { name = "prometheus", parameters = "0.0.0.0:9153" },
            # Blacklist hosts on the denylist
            {
              name        = "hosts",
              parameters  = "/denylist/hosts.denylist",
              configBlock = <<-EOF
                reload 3600s
                no_reverse
                fallthrough
              EOF
            },
            # Forward requests to the node resolver
            { name = "forward", parameters = ". 8.8.8.8" },
            { name = "cache", parameters = 30 },
            { name = "loop" },
            { name = "reload" },
            { name = "loadbalance" },
          ]
        },
        # Static zone.
        {
          zones = [{ zone = var.domain }]
          port  = 53
          plugins = [
            { name = "log" },
            { name = "file", parameters = "/etc/coredns/${var.domain}.db ${var.domain}" },
          ]
        },
        # Static zone.
        {
          zones = [{ zone = local.static_domain }]
          port  = 53
          plugins = [
            { name = "log" },
            { name = "file", parameters = "/etc/coredns/${local.static_domain}.db ${local.static_domain}" },
          ]
        },
        # K8s local zone.
        {
          zones = [{ zone = local.k8s_domain }]
          port  = 53
          plugins = [
            { name = "log" },
            { name = "file", parameters = "/etc/coredns/${local.k8s_domain}.db ${local.k8s_domain}" },
          ]
        },
        # All of the below logic is in service of setting up a fully-qualified
        # DHCP-host zone, despite the badly-configured router only responding
        # for unqualified names.
        {
          zones = [{ zone = local.dhcp_domain }]
          port  = 53
          plugins = [
            # Use the rewrite plugin to strip the dhcp domain from any incoming
            # requests, and forward them to the other zone.
            {
              name        = "rewrite",
              parameters  = "stop",
              configBlock = <<-EOT
              name regex (.*)\.${local.dhcp_domain_escaped} {1}
              EOT
            },
            { name = "forward", parameters = ". 127.0.0.1:10053" },
            { name = "loop" },
            # We override the log format so that we can identify this as the LAN DHCP resolver.
            { name = "log", parameters = ". \"{remote}:{port} - {>id} LAN DHCP \\\"{type} {class} {name} {proto} {size} {>do} {>bufsize}\\\" {rcode} {>rflags} {rsize} {duration}\"" },
            { name = "debug" },
          ]
        },
        # We use a separate zone for improved logging.
        {
          zones = [{ zone = "." }]
          port  = 10053
          plugins = [
            # We override the log format so that we can identify this as the FWD DHCP resolver.
            { name = "log", parameters = ". \"{remote}:{port} - {>id} FWD DHCP ${var.dhcp_resolver} \\\"{type} {class} {name} {proto} {size} {>do} {>bufsize}\\\" {rcode} {>rflags} {rsize} {duration}\"" },
            { name = "forward", parameters = ". ${var.dhcp_resolver}" },
          ]
        }
      ]
      zoneFiles = [
        {
          filename = "${local.static_domain}.db"
          domain   = local.static_domain
          contents = <<-EOF
            ${local.static_domain}. IN SOA sns.dns.icann.org. noc.dns.icann.org. 2015082541 7200 3600 1209600 3600
            %{for name, ip in var.static_ips}
            ${name}.${local.static_domain}. IN A ${ip}
            %{endfor}
          EOF
        },
        {
          filename = "${var.domain}.db"
          domain   = local.k8s_domain
          contents = <<-EOF
            ${var.domain}. IN SOA sns.dns.icann.org. noc.dns.icann.org. 2015082541 7200 3600 1209600 3600
            ${var.domain}. IN A ${var.k8s_ingress_ip}
            *.${var.domain}. IN A ${var.k8s_ingress_ip}
          EOF
        },
        {
          filename = "${local.k8s_domain}.db"
          domain   = local.k8s_domain
          contents = <<-EOF
            ${local.k8s_domain}. IN SOA sns.dns.icann.org. noc.dns.icann.org. 2015082541 7200 3600 1209600 3600
            *.${local.k8s_domain}. IN A ${var.k8s_ingress_ip}
          EOF
        },
      ]
    }),
  ]
}


data "kubernetes_service" "coredns_service" {
  metadata {
    name      = "${helm_release.coredns.name}-${helm_release.coredns.chart}"
    namespace = kubernetes_namespace.coredns.metadata[0].name
  }
}

output "dns_servers" {
  value = [for ingress in
    one(one(data.kubernetes_service.coredns_service.status).load_balancer).ingress :
    ingress.ip
  ]
}

output "domains" {
  value = {
    root   = var.domain
    k8s    = local.k8s_domain
    static = local.static_domain
  }
}
