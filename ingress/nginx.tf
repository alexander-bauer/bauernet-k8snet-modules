data "kubernetes_namespace" "kube_system" {
  metadata {
    name = "kube-system"
  }
}

resource "kubectl_manifest" "default_certificate" {
  yaml_body = yamlencode({
    apiVersion = "cert-manager.io/v1"
    kind       = "Certificate"
    metadata = {
      name      = "ingress-default"
      namespace = one(data.kubernetes_namespace.kube_system.metadata).name
    }
    spec = {
      secretName = "ingress-default-tls"
      issuerRef = {
        kind = "ClusterIssuer"
        name = var.default_certificate_issuer
      }
      dnsNames = [
        "*.${var.domain}" # wildcard
      ]
    }
  })
}

#data "kubernetes_secret" "default_certificate_tls" {
#  metadata {
#    namespace = one(data.kubernetes_namespace.kube_system.metadata).name
#    name      = yamldecode(kubectl_manifest.default_certificate.yaml_body_parsed).spec.secretName
#  }
#}

locals {
  default_certificate_ref = join("/", [
    #    one(data.kubernetes_secret.default_certificate_tls.metadata).namespace,
    #    one(data.kubernetes_secret.default_certificate_tls.metadata).name,
    one(data.kubernetes_namespace.kube_system.metadata).name,
    yamldecode(kubectl_manifest.default_certificate.yaml_body_parsed).spec.secretName,
  ])
}

resource "helm_release" "nginx_ingress_controller" {
  name       = "nginx"
  repository = "https://kubernetes.github.io/ingress-nginx"
  chart      = "ingress-nginx"
  namespace  = one(data.kubernetes_namespace.kube_system.metadata).name

  values = [
    yamlencode({
      controller = {
        service = {
          # FIXME: metallb with local traffic policy on a shared IP has implications for being able to route traffic
          #externalTrafficPolicy = "Local" # understood by Metallb to pass real source IP as header
          annotations = var.service_sharing_key != null ? {
            "metallb.universe.tf/allow-shared-ip" : var.service_sharing_key
          } : {}
        }
        extraArgs = {
          "default-ssl-certificate" : local.default_certificate_ref
        }
        watchIngressWithoutClass = true
        ingressClassResource = {
          enabled = false
        }
        kind = "DaemonSet"
        hostPort = {
          enabled = true
        }
        enableOIDC = true # permit OIDC in policies
        metrics = {
          enabled = true
          service = {
            annotations = {
              "prometheus.io/scrape" = "true"
              "prometheus.io/port"   = "10254"
            }
          }
        }
      }
    })
  ]
}
