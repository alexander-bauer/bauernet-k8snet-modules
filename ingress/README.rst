Ingress
=======

This component is responsible for configuring an ingress controller, which is
itself responsible for safely proxying inbound requests (mostly via HTTP/HTTPS,
but some via raw TCP proxying) to service pods.

The ingress controller in use is `Ingress NGINX
<https://github.com/kubernetes/ingress-nginx>`_. [#nginx]_ It is configured as
a daemonset, so that any cluster node is able to receive inbound traffic, and
route it properly.

It is configured with a default wildcard TLS certificate matching ``*.<root
domain>``, so that requests to unconfigured domains can still receive a valid
TLS response.

Terragrunt Configuration
------------------------

.. literalinclude:: terragrunt.hcl
  :caption: ``terragrunt.hcl``

Terraform Configuration
-----------------------

.. literalinclude:: nginx.tf
  :caption: ``nginx.tf``

.. rubric:: Footnotes

.. [#nginx] NGINX is pronounced "Engine X"
