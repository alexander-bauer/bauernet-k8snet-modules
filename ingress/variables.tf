variable "domain" {
  type = string
}

variable "service_sharing_key" {
  type    = string
  default = null
}

variable "default_certificate_issuer" {
  type = string
}
