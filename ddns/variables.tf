variable "cloudflare_token" {
  type      = string
  sensitive = true
}

variable "cloudflare_zone_id" {
  type      = string
  sensitive = true
}

variable "cloudflare_ddns_record_id" {
  type      = string
  sensitive = true
}

variable "cloudflare_ddns_record_name" {
  type = string
}
