resource "kubernetes_namespace" "ddns" {
  metadata {
    name = "ddns"
  }
}

resource "kubernetes_secret" "cloudflare_token" {
  metadata {
    name      = "cloudflare-token"
    namespace = kubernetes_namespace.ddns.metadata[0].name
  }

  data = {
    api_token = var.cloudflare_token
  }
}

resource "kubernetes_cron_job_v1" "cloudflare_ddns" {
  metadata {
    name      = "cloudflare-ddns"
    namespace = kubernetes_namespace.ddns.metadata[0].name
  }
  spec {
    schedule           = "0 * * * *"
    concurrency_policy = "Replace"
    job_template {
      metadata {
        name = "cloudflare-ddns-updater"
      }
      spec {
        template {
          metadata {
            name = "cloudflare-ddns-updater"
          }
          spec {
            active_deadline_seconds = 120
            container {
              name  = "cloudflare-ddns"
              image = "mirioeggmann/cloudflare-ddns:latest"
              env {
                name  = "ZONE_ID"
                value = var.cloudflare_zone_id
              }
              env {
                name  = "RECORD_ID"
                value = var.cloudflare_ddns_record_id
              }
              env {
                name  = "NAME"
                value = var.cloudflare_ddns_record_name
              }
              env {
                name  = "PROXIED"
                value = "false"
              }
              env {
                name = "API_TOKEN"
                value_from {
                  secret_key_ref {
                    name = kubernetes_secret.cloudflare_token.metadata[0].name
                    key  = "api_token"
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}
