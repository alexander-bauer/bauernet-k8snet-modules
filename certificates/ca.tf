variable "use_ca" {
  type        = bool
  description = "Create a CA and use it to sign certificates"
}

locals {
  selfsigned_issuer = var.use_ca ? nonsensitive(one(kubectl_manifest.cluster_issuer_selfsigned).name) : null
  ca_issuer         = var.use_ca ? nonsensitive(one(kubectl_manifest.cluster_issuer_ca).name) : null
  ca_certificate    = var.use_ca ? nonsensitive(one(data.kubernetes_secret.root_ca_certificate).data["tls.crt"]) : null
}

resource "kubectl_manifest" "cluster_issuer_selfsigned" {
  count      = var.use_ca ? 1 : 0
  depends_on = [helm_release.cert_manager]
  yaml_body = yamlencode({
    apiVersion = "cert-manager.io/v1"
    kind       = "ClusterIssuer"
    metadata = {
      name = "selfsigned"
    }
    spec = { selfSigned = {} }
  })
}

resource "kubectl_manifest" "root_ca" {
  count      = var.use_ca ? 1 : 0
  depends_on = [helm_release.cert_manager]
  yaml_body = yamlencode({
    apiVersion = "cert-manager.io/v1"
    kind       = "Certificate"
    metadata = {
      name      = "root-ca"
      namespace = one(kubernetes_namespace.cert_manager.metadata).name
    }
    spec = {
      isCA       = true
      commonName = "${var.domain}-ca"
      secretName = "ca-certificate"
      privateKey = {
        algorithm = "ECDSA"
        size      = 256
      }
      issuerRef = {
        name  = local.selfsigned_issuer
        kind  = "ClusterIssuer"
        group = "cert-manager.io"
      }
    }
  })
}

data "kubernetes_secret" "root_ca_certificate" {
  count = var.use_ca ? 1 : 0
  metadata {
    name      = yamldecode(one(kubectl_manifest.root_ca).yaml_body).spec.secretName
    namespace = one(kubernetes_namespace.cert_manager.metadata).name
  }
}

resource "kubectl_manifest" "cluster_issuer_ca" {
  count      = var.use_ca ? 1 : 0
  depends_on = [helm_release.cert_manager]
  yaml_body = yamlencode({
    apiVersion = "cert-manager.io/v1"
    kind       = "ClusterIssuer"
    metadata = {
      name = "ca"
    }
    spec = {
      ca = {
        secretName = one(one(data.kubernetes_secret.root_ca_certificate).metadata).name
      }
    }
  })
}
