resource "helm_release" "trust_manager" {
  name       = "trust-manager"
  repository = "https://charts.jetstack.io"
  chart      = "trust-manager"
  namespace  = one(kubernetes_namespace.cert_manager.metadata).name
}


resource "kubectl_manifest" "trust_bundle" {
  depends_on = [helm_release.trust_manager]
  yaml_body = yamlencode({
    apiVersion = "trust.cert-manager.io/v1alpha1"
    kind       = "Bundle"
    metadata = {
      name      = "trust-bundle"
      namespace = one(kubernetes_namespace.cert_manager.metadata).name
    }
    spec = {
      sources = concat(
        [{ useDefaultCAs = true }],
        !var.use_ca ? [] : [{ # only add if use_ca is true
          secret = {
            name = yamldecode(one(kubectl_manifest.root_ca).yaml_body).spec.secretName
            key  = "tls.crt"
          }
        }]
      )
      target = {
        namespaceSelector = {} # all namespaces
        configMap = {
          key = "bundle.pem"
        }
      }
    }
  })
}

# Output logic
output "bundle" {
  value = kubectl_manifest.trust_bundle.name
}
