variable "domain" {
  type = string
}

resource "kubernetes_namespace" "cert_manager" {
  metadata {
    name = "cert-manager"
  }
}

resource "helm_release" "cert_manager" {
  name       = "cert-manager"
  repository = "https://charts.jetstack.io"
  chart      = "cert-manager"
  namespace  = one(kubernetes_namespace.cert_manager.metadata).name

  values = [
    yamlencode({
      installCRDs = true
      podDnsConfig = {
        nameservers = ["1.1.1.1", "8.8.8.8"]
      }
    })
  ]
}

locals {
  default_issuer = coalesce(local.letsencrypt_prod_issuer, local.ca_issuer)
}

# Output logic
output "issuer" {
  value = local.default_issuer
}

output "letsencrypt" {
  value = {
    prod    = local.letsencrypt_prod_issuer
    staging = local.letsencrypt_staging_issuer
  }
}

output "ca" {
  value = {
    issuer      = local.ca_issuer
    certificate = local.ca_certificate
  }
}

output "annotations" {
  description = "Ingress annotation for using default certificate issuer"
  value = {
    "cert-manager.io/cluster-issuer" = local.default_issuer
  }
}
