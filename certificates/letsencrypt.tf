variable "use_letsencrypt" {
  type        = bool
  description = "Enable LetsEncrypt configuration"
}

variable "acme_email" {
  description = "Email address to use for LetsEncrypt purposes."
  type        = string
  default     = null
}

variable "cloudflare_api_token" {
  description = "Cloudflare API token for DNS operations"
  type        = string
  default     = null
  sensitive   = true
}

locals {
  # If use_letsencrypt is false, we skip setting this entire block, because it
  # will not be used, and thereby avoids referencing variables we should not
  # need to set.
  common_acme = !var.use_letsencrypt ? null : {
    email = var.acme_email
    solvers = [
      {
        dns01 = {
          cloudflare = {
            apiTokenSecretRef = {
              name = one(one(kubernetes_secret.cloudflare_api_token).metadata).name
              key  = one(keys(one(kubernetes_secret.cloudflare_api_token).data))
            }
          }
        }
      },
      { http01 = { ingress = { serviceType = "ClusterIP" } } }
    ]
  }

  staging_acme = merge(local.common_acme, {
    server              = "https://acme-staging-v02.api.letsencrypt.org/directory"
    privateKeySecretRef = { name = "letsencrypt-staging-key" }
  })

  prod_acme = merge(local.common_acme, {
    server              = "https://acme-v02.api.letsencrypt.org/directory"
    privateKeySecretRef = { name = "letsencrypt-key" }
  })

  letsencrypt_prod_issuer    = var.use_letsencrypt ? nonsensitive(one(kubectl_manifest.cluster_issuer_letsencrypt).name) : null
  letsencrypt_staging_issuer = var.use_letsencrypt ? nonsensitive(one(kubectl_manifest.cluster_issuer_letsencrypt_staging).name) : null
}

resource "kubernetes_secret" "cloudflare_api_token" {
  count = var.use_letsencrypt ? 1 : 0
  metadata {
    namespace = one(kubernetes_namespace.cert_manager.metadata).name
    name      = "cloudflare-api-token"
  }

  type = "Opaque"
  data = {
    api-token = one(compact([var.cloudflare_api_token])) # idiom to assert it's not null
  }
}


resource "kubectl_manifest" "cluster_issuer_letsencrypt_staging" {
  count      = var.use_letsencrypt ? 1 : 0
  depends_on = [helm_release.cert_manager]
  yaml_body = yamlencode({
    apiVersion = "cert-manager.io/v1"
    kind       = "ClusterIssuer"
    metadata = {
      name = "letsencrypt-staging"
    }
    spec = { acme = local.staging_acme }
  })
}

resource "kubectl_manifest" "cluster_issuer_letsencrypt" {
  count      = var.use_letsencrypt ? 1 : 0
  depends_on = [helm_release.cert_manager]
  yaml_body = yamlencode({
    apiVersion = "cert-manager.io/v1"
    kind       = "ClusterIssuer"
    metadata = {
      name = "letsencrypt"
    }
    spec = { acme = local.prod_acme }
  })
}
