Certificates
============

This component is responsible for configuring ``cert-manager`` for retrieving
valid TLS certificates. Certificates consumed by this service are primarily server certificates for use in HTTPS or other TLS services which answer clients like browsers.

Terragrunt Configuration
------------------------

.. literalinclude:: terragrunt.hcl
  :caption: ``terragrunt.hcl``

Terraform Configuration
-----------------------

.. literalinclude:: cert-manager.tf
  :caption: ``cert-manager.tf``

.. literalinclude:: letsencrypt.tf
  :caption: ``letsencrypt.tf``

.. literalinclude:: ca.tf
  :caption: ``ca.tf``
