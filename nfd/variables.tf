variable "namespace" {
  description = "Namespace to create for node-feature-discovery"
  type        = string
  default     = "node-feature-discovery"
}

variable "chart_version" {
  description = "Chart version to use"
  type        = string
  default     = "0.13.4"
}

variable "custom_features" {
  description = "Custom node features to discover"
  type = list(object({
    name    = string
    matchOn = list(any)
  }))
  default = []
}
