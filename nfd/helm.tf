resource "kubernetes_namespace" "this" {
  metadata {
    name = var.namespace
  }
}

resource "helm_release" "nfd" {
  name       = "node-feature-discovery"
  repository = "https://kubernetes-sigs.github.io/node-feature-discovery/charts"
  chart      = "node-feature-discovery"
  namespace  = one(kubernetes_namespace.this.metadata).name
  version    = var.chart_version

  values = [jsonencode({
    worker = {
      config = {
        sources = {
          custom = var.custom_features
        }
      }
    }
  })]
}
