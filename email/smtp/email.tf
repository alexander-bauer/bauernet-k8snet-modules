resource "kubernetes_namespace" "mail" {
  metadata {
    name = "mail"
  }
}

variable "mail_domain" {
  type = string
}
variable "mail_from_address" {
  type        = string
  default     = null
  description = "Override every from address, used if relaying through one account"
}
variable "mail_from_pattern" {
  type = string
}
variable "mail_relay" {
  type = object({
    host     = string
    port     = number
    username = string
    password = string
  })
}

resource "helm_release" "mail" {
  name       = "mail"
  repository = "https://bokysan.github.io/docker-postfix/"
  chart      = "mail"
  namespace  = kubernetes_namespace.mail.metadata[0].name

  values = [
    yamlencode({
      config = {
        general = {
          ALLOWED_SENDER_DOMAINS = var.mail_domain
          MASQUERADED_DOMAINS    = var.mail_domain # rewrite all from addresses
          RELAYHOST              = "${var.mail_relay.host}:${var.mail_relay.port}"
          RELAYHOST_USERNAME     = var.mail_relay.username
          RELAYHOST_PASSWORD     = var.mail_relay.password
        }
      }
    })
  ]
}

output "domain" {
  value = var.mail_domain
}

output "pattern" {
  value = {
    from = coalesce(var.mail_from_address, "%s@${var.mail_domain}")
    # Capital "From" is the formatted from sender
    From = var.mail_from_pattern
  }
}

output "smtp" {
  value = {
    host     = "mail.${one(kubernetes_namespace.mail.metadata).name}.svc.cluster.local"
    port     = 587
    tls      = false
    username = ""
    password = ""
  }
}
