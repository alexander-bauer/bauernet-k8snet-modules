Email
=====

This component configures a mail relay for use by internal applications. It is
responsible for performing authentication to an external public relay, and
thereby getting arbitrary email delivered.

The outputs of this component can be used by other Kubernetes services to
direct mail properly.

Terragrunt Configuration
------------------------

.. literalinclude:: terragrunt.hcl
  :caption: ``terragrunt.hcl``

Terraform Configuration
-----------------------

.. literalinclude:: email.tf
   :caption: ``email.tf``
