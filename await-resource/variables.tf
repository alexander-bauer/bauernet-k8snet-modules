variable "namespace" {
  description = "Namespace in which to await the resource"
  type        = string
}

variable "kind" {
  description = "Resource kind"
  type        = string
}

variable "name" {
  description = "Resource name"
  type        = string
}

variable "triggers" {
  description = "Triggers to use to wait"
  type        = map(any)
}

variable "sleep_before" {
  description = "Number of seconds to sleep before beginning kubectl wait"
  type        = number
  default     = 0
}

variable "wait_for" {
  description = "Argument to the kubectl wait --for flag; if unset, value may be inferred from kind"
  type        = string
  default     = null
}

variable "timeout" {
  description = "Set timeout for kubectl wait"
  type        = number
  default     = 0
}
