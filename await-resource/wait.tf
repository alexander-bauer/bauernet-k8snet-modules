locals { # Resolve common default --for arguments based on the Kind. For example, it is typical
  # to wait for Buckets to reach phase=Bound.
  default_wait_for = {
    Bucket = "jsonpath=.status.phase=Bound"
  }

  # Resolve to argument or default.
  wait_for = var.wait_for == null ? local.default_wait_for[var.kind] : var.wait_for
}

resource "null_resource" "sleep_before" {
  triggers = var.triggers
  provisioner "local-exec" {
    command = "sleep ${var.sleep_before}"
  }
}

resource "null_resource" "wait_for" {
  depends_on = [null_resource.sleep_before]
  triggers   = var.triggers
  provisioner "local-exec" {
    command = join(" ", [
      "kubectl wait",
      "--for=${local.wait_for}",
      "--namespace=${var.namespace}",
      "--timeout=${var.timeout}",
      "${var.kind}/${var.name}"
    ])
  }
}
