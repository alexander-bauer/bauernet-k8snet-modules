#######################
BauerNet K8SNet Modules
#######################

This repository contains Terraform modules related to Kubernetes network services, such
as the ingress controller, DNS server, email relay, and certificate management.

.. toctree::
   :maxdepth: 1

   */README
   CHANGELOG
