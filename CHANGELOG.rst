#########
Changelog
#########

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog <https://keepachangelog.com/en/1.0.0/>`_,
and this project adheres to `Semantic Versioning <https://semver.org/spec/v2.0.0.html>`_.

[Unreleased]
============

[0.11.0] - 2023-09-09
=====================

Added
-----

* OAuth2-Proxy module support for ``external-service`` module.

[0.10.0] - 2023-09-09
=====================

Added
-----

* Node feature discovery

[0.9.0] - 2023-07-13
====================

Added
-----

* DNS entry for root domain to direct to ingress controller

[0.8.0] - 2023-06-19
====================

Added
-----

* Enabled metrics for NGINX Ingress controller

Changed
-------

* Unified ``versions.tf`` files using the symlink pattern
* Moved variables in ``ingress`` module to ``variables.tf``

Fixed
-----

* Adjusted missing ``helm`` provider version declaration

[0.7.0] - 2023-06-19
====================

Added
-----

* Module ``ddns``

[0.6.0] - 2023-05-27
====================

Added
-----

* Argument ``timeout`` for ``kubectl wait`` in ``await-resource`` module

[0.5.0] - 2023-05-27
====================

Added
-----

* Argument ``sleep_before`` to wait prior to ``kubectl wait``

[0.4.0] - 2023-05-15
====================

Added
-----

* ``await-resource`` module for using ``kubectl wait`` for resources

[0.3.0] - 2023-05-04
====================

Added
-----

* ``external-service`` module for general-purpose exposure of outside-cluster services

[0.2.0] - 2023-05-02
====================

Changed
-------

* Removed input parameter ``storage_class`` from ``email/smtp`` which was causing issues

[0.1.0] - 2023-05-02
====================

Added
-----

* Migrated initial modules from ``bauernet-original``:
  * ``certificates``
  * ``dns``
  * ``email/smtp``
  * ``ingress``
