locals {
  ingress_port = length(var.ports) == 1 ? one(var.ports) : var.ingress_port_name == null ? null : one([
    for port in var.ports : port if port.name == var.ingress_port_name
  ])

  ingress_annotations = merge(var.ingress_annotations, !var.enable_auth ? {} :
    one(module.auth).annotations.nginx.role[local.auth_role_name]
  )
}

resource "kubernetes_ingress_v1" "external" {
  count = var.ingress_hostname != null ? 1 : 0
  metadata {
    name        = "external"
    namespace   = local.namespace
    annotations = local.ingress_annotations
  }
  spec {
    rule {
      host = var.ingress_hostname
      http {
        path {
          path = "/"
          backend {
            service {
              name = one(kubernetes_service.external.metadata).name
              port {
                number = local.ingress_port != null ? local.ingress_port.port : null
              }
            }
          }
        }
      }
    }
    dynamic "tls" {
      for_each = var.ingress_tls ? [1] : []
      content {
        secret_name = "${var.name}-tls"
        hosts       = [var.ingress_hostname]
      }
    }
  }
}
