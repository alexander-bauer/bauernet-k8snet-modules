variable "namespace" {
  description = "New or existing namespace into which to place resources"
  type        = string
}

variable "namespace_exists" {
  description = "Skip creating the namespace before other resources"
  type        = bool
  default     = false
}

variable "name" {
  description = "Name for resources"
  type        = string
}

variable "ingress_hostname" {
  description = "Hostname to use for the ingress. Ingress is not created if omitted."
  type        = string
  default     = null
}

variable "ingress_annotations" {
  description = "Annotations to apply to the ingress. Ignored if ingress not enabled."
  type        = map(string)
  default     = {}
}

variable "ingress_port_name" {
  description = "Name of port from var.ports to route to"
  type        = string
  default     = null
}

variable "ingress_tls" {
  description = "Whether to enable TLS on the ingress"
  type        = bool
  default     = true
}

variable "ip_addresses" {
  description = "List of IPs to expose as endpoints"
  type        = list(string)
  #validation {
  #  condition = alltrue([
  #    for ip in var.ip_addresses : can(regex(join("", [
  #      "^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)",
  #      "(?:=\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}",
  #    ]), ip))
  #  ])
  #  error_message = "Invalid IP address provided."
  #}
}

variable "ports" {
  description = "List of ports to expose in the service"
  type = list(object({
    name     = string
    protocol = string
    port     = number
  }))
}

variable "enable_auth" {
  description = "Enable oauth2-proxy client authentication"
  type        = bool
  default     = false
}

variable "auth_keycloak_realm_name" {
  description = "Keycloak realm name, required if enable_auth is set"
  type        = string
  default     = null
}

variable "auth_proxy_url" {
  description = "Auth proxy URL base, required if enable_auth is set"
  type        = string
  default     = null
}
