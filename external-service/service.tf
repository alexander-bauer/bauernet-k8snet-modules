resource "kubernetes_endpoints" "external" {
  metadata {
    name      = var.name
    namespace = local.namespace
  }

  subset {
    dynamic "address" {
      for_each = var.ip_addresses
      content {
        ip = address.value
      }
    }
    dynamic "port" {
      for_each = var.ports
      content {
        name     = port.value.name
        protocol = port.value.protocol
        port     = port.value.port
      }
    }
  }
}

resource "kubernetes_service" "external" {
  metadata {
    name      = var.name
    namespace = local.namespace
  }

  spec {
    dynamic "port" {
      for_each = var.ports
      content {
        protocol    = port.value.protocol
        name        = port.value.name
        port        = port.value.port
        target_port = port.value.port
      }
    }
  }
}
