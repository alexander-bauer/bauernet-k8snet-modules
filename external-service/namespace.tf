resource "kubernetes_namespace" "external" {
  count = var.namespace_exists == true ? 0 : 1
  metadata {
    name = var.namespace
  }
}

locals {
  # We use a local to trick the dependency system. Other resources are created in
  # local.namespace, and that string value either depends on
  # kubernetes_namespace.external or is filled directly from the variable.
  namespace = var.namespace_exists ? one(one(kubernetes_namespace.external).metadata).name : var.namespace
}
