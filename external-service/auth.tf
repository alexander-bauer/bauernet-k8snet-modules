locals {
  auth_role_name = "${var.name}-fullaccess"
}
# If auth is enabled, use the oauth2-proxy subclient module to set up a forwarding rule
# and a role.
module "auth" {
  count  = var.enable_auth ? 1 : 0
  source = "git::https://gitlab.com/alexander-bauer/bauernet-authzn-modules//keycloak/oauth2-proxy-subclient?ref=v0.6.0"

  realm    = var.auth_keycloak_realm_name
  base_url = var.auth_proxy_url
  roles = {
    (local.auth_role_name) = {
      description = "Access via OAuth2-Proxy to ${var.name}"
    }
  }
}
